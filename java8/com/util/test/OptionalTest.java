package com.util.test;

import java.util.Optional;

import org.testng.annotations.Test;

public class OptionalTest {
	
	private Optional<String> optional;
	
	/**
	 * 此方法用于比较of,ofNullable,empty创建实例的异同
	 * 用of创建实例不能传入null；
	 * 用ofNullable创建实例进行判断，如果传入非null值，则调用of方法创建实例,否则若传入null，则调用empty方法创建实例；
	 * 用empty创建实例默认传入null，isPresent方法一定是false
	 * isPresent方法用于判断此Optional包装的实例是否是null，是null则返回false，否则返回true
	 */
	@Test
	public void createInstanceTest() {
		
		try {
			optional=Optional.of(null);
		}catch(NullPointerException e) {
			System.out.println("Optional.of(null) will cause NullPointerException");
		}
		
		optional=Optional.empty();
		System.out.println("Optional.empty(): " +optional+"  "+optional.isPresent());
		
		optional=Optional.ofNullable(null);
		System.out.println("Optional.ofNullable(x):" +optional+"  "+optional.isPresent());
		
		optional=Optional.ofNullable("1122");
		System.out.println("Optional.ofNullable(x):" +optional+"  "+optional.isPresent());
		
	}
	
	@Test
	public void basicTest() {
		System.out.println("Is Present:"+optional.isPresent());
		System.out.println("");
	}
	
	@Test
	public void advanceTest() {
		optional=Optional.ofNullable(null);
		//String test=optional.orElse("orElse");
		System.out.println(optional.orElse("wkk"));
		System.out.println(optional.orElseGet(()->"wkkGet"));
	}
	
}
